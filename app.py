from flask import Flask, render_template, request, make_response, g
import os
import socket
import random
import json
import logging
import requests

hostname = socket.gethostname()

app = Flask(__name__)

gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.INFO)

@app.route("/", methods=['POST','GET'])
def hello():

    api_url = "http://api/info"
    response = requests.get(api_url)
    
    resp = make_response(render_template(
        'index.html',
        hostname=hostname,
        info=response.json(),
    ))
    return resp


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
